syntax on
set autoindent
set cindent
set tabstop=4
set shiftwidth=4
set softtabstop=4
set noexpandtab
set nu

let mapleader=";"

noremap <Leader>v :vsplit 
noremap <Leader>h :split 
noremap <Leader>t :tabe 
noremap <Leader>n <ESC>:tabn<CR>
noremap <Leader>p <ESC>:tabp<CR>
noremap <Leader>s :w<CR>
noremap <Leader>q :q<CR>
noremap <Leader>c :!clear && make<CR>
noremap <Leader>C :!clear && make && ./a.out

imap <Leader><Tab> <C-n>
imap jj <Esc>
